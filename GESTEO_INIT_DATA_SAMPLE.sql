USE gesteo;

INSERT INTO ville(id, code_postal, ville)
VALUES
    (1, '33600', 'PESSAC'),
    (2, '33700', 'MERIGNAC'),
    (3, '33000', 'BORDEAUX'),
    (4, '06100', 'NICE'),
    (5, '64510', 'PAU'),
    (6, '33115', 'PYLA / MER'),
    (7, '47210', 'SAINT CIER / GIRONDE'),
    (8, '69001', 'LYON CEDEX 1'),
    (9, '84110', 'VAISON LA ROMAINE'),
    (10, '17154', 'LA ROCHELLE');


INSERT INTO client(id, nom, adresse1, adresse2, id_cp_ville)
VALUES (1, 'AVALONE', '152 Avenue Jean-Jaurès', null, 1),
       (2, 'CGI', null, null, null),
       (3, 'EPSI', null, null, null),
       (4, 'CAP GEMINI', null, null, null),
       (5, 'EDF', null, null, null),
       (6, 'ORANGE', null, null, null),
       (7, 'SFR', null, null, null);


INSERT INTO fonction(id, libelle)
VALUES (1, 'Gérant'),
       (2, 'Directeur'),
       (3, 'Technicien'),
       (4, 'Commercial'),
       (5, 'Intervenant'),
       (6, 'Consultant');


INSERT INTO personne(id, nom, prenom, telephone, email)
VALUES (1, 'NAPO', 'Léon', '0603457118', 'n.leon@empire.com'),
       (2, 'LE', 'Client', null, 'le.client@leclient.com'),
       (3, 'LE TIMONNIER', 'Marius', '0649233244', 'marius.letimonnier@marin.com'),
       (4, 'CHATAIGNE', 'Sebastien', null, 'sebchataigne@la-foret.fr'),
       (5, 'RENO', 'Jean', null, 'reno.jean@cinema.fr');


INSERT INTO appartient(id_personne, id_fonction, id_client)
VALUES (1, 1, 1),
       (2, 3, 2),
       (3, 3, 3),
       (4, 4, 4),
       (5, 6, 6),
       (1, 5, 2);


INSERT INTO type_materiel(id, libelle)
VALUES (1, 'Imprimante'),
       (2, 'Switch'),
       (3, 'Routeur'),
       (4, 'Serveur'),
       (5, 'Ordinateur');


INSERT INTO materiel(id, libelle, numero_de_serie, id_client, id_type)
VALUES (1, 'Dell 1520', '011201252012', 1, 1),
       (2, 'NETGEAR DS1008', '053518613548', 1, 2),
       (3, 'NETGEAR DS1005', '35s4d54s684f', 1, 2),
       (4, 'TP-LINK GS108', 'sdfsdf435438', 1, 3),
       (5, 'PASSERELLE LINUX', 'fhh64zeer64fg', 1, 4),
       (6, 'DEV NICOLAS', '6784f8dgd38', 1, 4);


INSERT INTO type_interface(id, libelle)
VALUES (1, 'LAN'),
       (2, 'WAN'),
       (3, 'BLUETOOTH'),
       (4, 'WLAN');


INSERT INTO type_affectation(id, libelle)
VALUES (1, 'DHCP'),
       (2, 'STATIC');


INSERT INTO interface(id, nom, mac, id_type, id_materiel)
VALUES (1, 'eth0', '01:02:02:02:02', 1, 5),
       (2, 'eth1', '01:02:02:02:03', 1, 5),
       (3, 'wl0' , '18:1F:01:01:01', 4, 4),
       (4, 'eth0', '18:1F:01:01:02', 2, 4),
       (5, 'eth0', 'E6:1F:01:01:01', 1, 3),
       (6, 'eth1', 'E6:1F:01:01:02', 1, 3),
       (7, 'eth2', 'E6:1F:01:01:03', 1, 3),
       (8, 'eth3', 'E6:1F:01:01:04', 1, 3);


INSERT INTO adresse_ip(id, ipV4, ipV6, masque, id_interface, id_type_affectation)
VALUES (1, '192.168.254.250', null, '255.255.255.0'  , 1, 1),
       (2, '17.10.0.1'      , null, '255.255.255.240', 2, 1),
       (3, '192.168.251.254', null, '255.255.255.0'  , 3, 1),
       (4, '192.168.254.251', null, '255.255.255.240', 4, 1),
       (5, '192.168.254.10' , null, '255.255.255.0'  , 5, 2),
       (6, '192.168.254.11' , null, '255.255.255.0'  , 6, 2),
       (7, '192.168.254.12' , null, '255.255.255.0'  , 7, 2),
       (8, '192.168.254.13' , null, '255.255.255.0'  , 8, 2);