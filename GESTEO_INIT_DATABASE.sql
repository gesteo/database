DROP DATABASE IF EXISTS 	gesteo;
CREATE DATABASE 			gesteo;
USE 						gesteo;

ALTER DATABASE 				gesteo 
CHARACTER SET 				utf8 
COLLATE 					utf8_unicode_ci;

CREATE TABLE ville (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    code_postal 			CHAR(5) 		NOT NULL,
    ville 					VARCHAR(100) 	NOT NULL
)Engine=InnoDB;


CREATE TABLE client (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    nom 					VARCHAR(50) 	NOT NULL,
    adresse1 				VARCHAR(250) 	DEFAULT NULL,
    adresse2 				VARCHAR(250) 	DEFAULT NULL,
    id_cp_ville 			INT 			DEFAULT NULL,
    CONSTRAINT FK_CLIENT_VILLE 				FOREIGN KEY (id_cp_ville) 			REFERENCES ville(id)
)Engine=InnoDB;


CREATE TABLE fonction (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    libelle 				VARCHAR(50) 	NOT NULL
)Engine=InnoDB;


CREATE TABLE personne (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    nom 					VARCHAR(50) 	NOT NULL,
    prenom 					VARCHAR(50) 	NOT NULL,
    telephone 				VARCHAR(14) 	DEFAULT NULL,
    email 					VARCHAR(100) 	NOT NULL
)Engine=InnoDB;


CREATE TABLE appartient (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    id_personne 			INT 			NOT NULL,
    id_fonction 			INT 			NOT NULL,
    id_client 				INT 			NOT NULL,
    CONSTRAINT FK_APPARTIENT_CLIENT 		FOREIGN KEY (id_client) 			REFERENCES client(id),
    CONSTRAINT FK_APPARTIENT_FONCTION 		FOREIGN KEY (id_fonction) 			REFERENCES fonction(id),
    CONSTRAINT FK_APPARTIENT_PERSONNE 		FOREIGN KEY (id_personne) 			REFERENCES personne(id)
)Engine=InnoDB;


CREATE TABLE type_materiel (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    libelle 				VARCHAR(50) 	NOT NULL
)Engine=InnoDB;


CREATE TABLE materiel (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    libelle 				VARCHAR(100) 	NOT NULL,
    numero_de_serie 		VARCHAR(30) 	NOT NULL,
    id_client 				INT 			NOT NULL,
    id_type 				INT 			NOT NULL,
    CONSTRAINT FK_MATERIEL_CLIENT 			FOREIGN KEY (id_client) 			REFERENCES client(id),
    CONSTRAINT FK_MATERIEL_TYPE 			FOREIGN KEY (id_type) 				REFERENCES type_materiel(id)
)Engine=InnoDB;


CREATE TABLE type_interface (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    libelle 				VARCHAR(10) 	NOT NULL
)Engine=InnoDB;


CREATE TABLE type_affectation (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    libelle 				VARCHAR(10) 	NOT NULL
)Engine=InnoDB;


CREATE TABLE interface (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    nom 					VARCHAR(10) 	NOT NULL,
    mac 					VARCHAR(14) 	DEFAULT '00:00:00:00:00',
    id_type 				INT 			NOT NULL,
    id_materiel 			INT 			NOT NULL,
    CONSTRAINT FK_TYPE_INTERFACE 			FOREIGN KEY (id_type) 				REFERENCES type_interface(id),
    CONSTRAINT FK_INTERFACE_MATERIEL 		FOREIGN KEY (id_materiel) 			REFERENCES materiel(id)
)Engine=InnoDB;


CREATE TABLE adresse_ip (
    id 						INT 			NOT NULL PRIMARY KEY auto_increment,
    ipV4 					VARCHAR(15) 	NOT NULL,
    ipV6 					VARCHAR(100) 	DEFAULT NULL,
    masque 					VARCHAR(15) 	NOT NULL DEFAULT '255.255.255.0',
    id_interface 			INT 			NOT NULL,
    id_type_affectation 	INT 			NOT NULL,
    CONSTRAINT FK_ADRESSE_INTERFACE 		FOREIGN KEY (id_interface) 			REFERENCES interface(id),
    CONSTRAINT FK_ADRESSE_TYPE_AFFECTATION 	FOREIGN KEY (id_type_affectation) 	REFERENCES type_affectation(id)
)Engine=InnoDB;
